//
//  HeroCollectionViewCell.swift
//  MarvelCollectionView
//
//  Created by Mikalai Kuts on 5/7/21.
//

import UIKit

final class HeroCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var heroLabel: UILabel!
    @IBOutlet weak var heroImage: UIImageView!
    private var heroUrl: String?
    private let shared: ImageService = ImageService.shared
    
    override func prepareForReuse() {
        super.prepareForReuse()
        heroImage.image = UIImage(named: "placeholder")
    }
    
    func configure(hero: Hero) {
        heroUrl = hero.heroImage.urlImage
        let urlString = "\(hero.heroImage.urlImage).\(hero.heroImage.typeOfImage)"
        heroLabel.text = hero.name
        shared.loadImage(urlString: urlString) { [weak self] (result) in
            switch result {
            case .success(let heroImage):
                if self?.heroUrl == hero.heroImage.urlImage {
                    DispatchQueue.main.async {
                        self?.heroImage.image = heroImage
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}

