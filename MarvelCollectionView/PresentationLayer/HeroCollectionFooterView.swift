//
//  HeroCollectionReusableView.swift
//  MarvelCollectionView
//
//  Created by Mikalai Kuts on 5/18/21.
//

import UIKit

final class HeroCollectionFooterView: UICollectionReusableView {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
}
