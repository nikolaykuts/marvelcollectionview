//
//  ViewController.swift
//  MarvelCollectionView
//
//  Created by Mikalai Kuts on 5/4/21.
//

import UIKit

final class HeroCollectionController: UIViewController {
    
    @IBOutlet private weak var heroCollection: UICollectionView!
    private let refreshControl = UIRefreshControl()
    private let searchController = UISearchController(searchResultsController: nil)
    private var activityIndicator = UIActivityIndicatorView()
    private let cellIdentifier = "heroCell"
    private let heroesService: HeroesService = HeroesService()
    private let footerIdentifier = "Footer"
    private let storyboardName = "Main"
    private let heroViewControllerIdentifier = "HeroViewController"
    private let offsetLabel: CGFloat = 8
    private let offsetImage: CGFloat = 15
    private var heroes = [Hero]()
    private var displayingHeroes = [Hero]()
    private var offset = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        setupSearchController()
        loadData()
        refreshControl.addTarget(self, action: #selector(pullToRefresh(_:)), for: .valueChanged)
    }
}

// MARK: - Private methods for HeroCollectionController
private extension HeroCollectionController {
    func setupSearchController() {
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
    }
    
    func setupCollectionView() {
        heroCollection.refreshControl = refreshControl
        heroCollection.dataSource = self
        heroCollection.delegate = self
    }
    
    // MARK:  Get array of heroes and update UI
    func loadData() {
        heroesService.getHeroes(offset: offset) { [weak self] (result) in
            switch result {
            case .success(let heroResponse):
                self?.heroes.append(contentsOf: heroResponse.data.results)
                self?.displayingHeroes.append(contentsOf: heroResponse.data.results)
                self?.offset = self?.heroes.count ?? 0
                DispatchQueue.main.async {
                    self?.heroCollection.reloadData()
                    self?.activityIndicator.stopAnimating()
                }
            case .failure(let error):
                print("Error:", error)
            }
        }
    }
    
    // MARK: Reload collection view
    @objc func pullToRefresh(_ sender: UIRefreshControl) {
        reload()
        sender.endRefreshing()
    }
    
    func reload() {
        loadData()
    }
    
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension HeroCollectionController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = view.frame.width
        let height: CGFloat = 290
        return CGSize(width: width/2-30, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return displayingHeroes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let heroCell = heroCollection.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as? HeroCollectionViewCell else { return UICollectionViewCell() }
        let heroCellWidth = heroCell.frame.width
        heroCell.configure(hero: displayingHeroes[indexPath.row])
        heroCell.heroLabel.frame.size.width = (heroCellWidth - offsetLabel)
        heroCell.heroImage.frame.size.width = (heroCellWidth - offsetImage)
        return heroCell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (displayingHeroes.count == heroes.count) && (indexPath.row == displayingHeroes.count - 1) {
            activityIndicator.startAnimating()
            loadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionFooter:
            guard let footer = heroCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerIdentifier, for: indexPath) as? HeroCollectionFooterView else { return UICollectionReusableView() }
            activityIndicator = footer.activityIndicator
            return footer
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        guard let heroViewController = storyboard.instantiateViewController(withIdentifier: heroViewControllerIdentifier) as? HeroViewController else { return }
        show(heroViewController, sender: nil)
    }
}

// MARK: - UISearchBarDelegate
extension HeroCollectionController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            displayingHeroes = heroes
        } else {
            displayingHeroes  = []
            for hero in heroes{
                if hero.name.contains(searchText) {
                    displayingHeroes.append(hero)
                }
            }
        }
        heroCollection.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        displayingHeroes = heroes
        searchBar.text = ""
        heroCollection.reloadData()
    }
}
