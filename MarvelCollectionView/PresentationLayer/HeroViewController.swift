//
//  HeroViewController.swift
//  MarvelCollectionView
//
//  Created by Mikalai Kuts on 5/15/21.
//

import UIKit

final class HeroViewController: UIViewController {
    
    var scrollView = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    func setupScrollView() {
        scrollView = UIScrollView(frame: self.view.bounds)
        self.view.addSubview(scrollView)
    }
    
}
