//
//  ImageService.swift
//  MarvelCollectionView
//
//  Created by Mikalai Kuts on 5/15/21.
//

import Foundation
import UIKit

enum ImageLoadingError: Error {
    case imageLoadingError
}

final class ImageService {
    
    private let cachedImages = NSCache<NSString,UIImage>()
    static var shared: ImageService = ImageService()
    
    private init() {}
    
    func loadImage(urlString: String, completion: @escaping(Result<UIImage, ImageLoadingError>) -> Void) {
        let nsKey = NSString(string: urlString)
        guard let url = URL(string: urlString) else { return }
        if let cachedImage = cachedImages.object(forKey: nsKey) {
            completion(.success(cachedImage))
        } else {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    print("No image by url \(error)")
                    completion(.failure(ImageLoadingError.imageLoadingError))
                }
                if let data = data {
                    guard let image = UIImage(data: data) else { return }
                    self.cachedImages.setObject(image, forKey: nsKey)
                    completion(.success(image))
                }
            }.resume()
        }
    }
}
