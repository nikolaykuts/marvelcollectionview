//
//  DataRetriver.swift
//  MarvelCollectionView
//
//  Created by Mikalai Kuts on 5/4/21.
//

import Foundation

final class HeroesService {
    
    func getHeroes(offset: Int, completion: @escaping (Result<HeroesResponse, Error>) -> Void) {
        let timeStamp: String = String(Date().timestamp)
        let hashString = { () -> String in
            let md5 = Md5()
            let hashValue = md5.getMd5(string: "\(timeStamp)\(AppConstant.privateKey)\(AppConstant.publicKey)")
            let hashValueHex = hashValue.map {String(format: "%02hhx", $0) }.joined()
            return hashValueHex
        }
        guard let urlString = URL(string: "\(AppConstant.apiUrl)characters?orderBy=-modified&limit=20&offset=\(offset)&ts=\(timeStamp)&apikey=\(AppConstant.publicKey)&hash=\(hashString())") else { return }
        URLSession.shared.dataTask(with: urlString) { (data, _, error) in
            if let error = error {
                print(error)
                completion(.failure(error))
                return
            }
            guard let data = data else { return completion(.failure(error!)) }
            do {
                let heroes = try JSONDecoder().decode(HeroesResponse.self, from: data)
                completion(.success(heroes))
            } catch let jsonError {
                completion(.failure(jsonError))
            }
        }.resume()
    }
}
