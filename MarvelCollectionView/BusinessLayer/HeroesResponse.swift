//
//  HeroesJsonDecoder.swift
//  MarvelCollectionView
//
//  Created by Mikalai Kuts on 5/4/21.
//

import Foundation

struct HeroesResponse: Codable {
    let data: DataHeroes
}

struct DataHeroes: Codable {
    var results: [Hero]
}

struct Hero: Codable {
    var id: Int
    var name: String
    var description: String
    var heroImage: HeroImage
    var comics: Comics
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case heroImage = "thumbnail"
        case comics
    }
}

struct Comics: Codable {
    var available: Int
    var items: [Item]
}

struct Item: Codable {
    var resourceUrl: String
    var name: String
    
    enum CodingKeys: String, CodingKey{
        case resourceUrl = "resourceURI"
        case name
    }
}

struct HeroImage: Codable {
    var urlImage: String
    var typeOfImage: String
    
    enum CodingKeys: String, CodingKey{
        case urlImage = "path"
        case typeOfImage = "extension"
    }
}
