//
//  ExtensionDate.swift
//  MarvelCollectionView
//
//  Created by Mikalai Kuts on 5/4/21.
//

import Foundation

// MARK: - Creating timestamp value
extension Date {
    var timestamp: Int {
        return Int(self.timeIntervalSince1970)
    }
}
