//
//  AppConstant.swift
//  MarvelCollectionView
//
//  Created by Mikalai Kuts on 5/15/21.
//

import Foundation

enum AppConstant {
    static let publicKey: String = "faa9fbb41ab18396cfb1f39a25d18138"
    static let privateKey: String = "493a670758a0d24cf6ebcdf7dcd2c192a3877ce2"
    static let apiUrl: String = "https://gateway.marvel.com:443/v1/public/"
}
