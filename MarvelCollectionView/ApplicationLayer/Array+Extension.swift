//
//  ExtensionArray.swift
//  MarvelCollectionView
//
//  Created by Mikalai Kuts on 5/15/21.
//

import Foundation

// MARK: - Safe array access
extension Array {
    public subscript(index: Int, default defaultValue: @autoclosure () -> Element) -> Element {
        guard index >= 0, index < endIndex else {
            return defaultValue()
        }

        return self[index]
    }
}
